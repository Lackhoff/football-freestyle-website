/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Praktikum 09 Aufgabe 1 - Dynamisches Formular
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

let button1 = document.querySelector("input[value='nopicture']");
let button2 = document.querySelector("input[value='picture']");

button2.addEventListener('click', () => {
  let p = document.createElement('p');
  p.id = 'file';
  document.querySelector('fieldset p:last-child').after(p);
  let label = document.createElement('label');
  document.querySelector('fieldset p:last-child').append(label);
  let text = document.createTextNode('Datei:');
  document.querySelector('fieldset p:last-child').append(text);
  let input = document.createElement('input');
  input.id = 'file';
  input.type = 'file';
  input.name = 'file';
  document.querySelector('fieldset p:last-child').append(input);
});

button1.addEventListener('click', () => {
  if (document.querySelector('fieldset p:last-child').id === 'file') {
    document.querySelector('fieldset p:last-child').remove();
  }
});
