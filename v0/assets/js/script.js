/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Praktikum 07 Aufgabe 3 - JavaScript-Vorbereitung
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
function getViewportWidth() {
  return window.innerWidth || document.documentElement.clientWidth;
}

console.log(`Die Viewport-Breite beträgt: ${getViewportWidth()} Pixel.`);

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Praktikum 08 Aufgabe 1 - Fachobjekte
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

function Kategorie(name, bildURL, bildName) {
  this.name = name;
  this.Bild = new Bild(bildURL, bildName);
}

function Tutorial(
  name,
  sprache,
  beschreibung,
  dauer,
  datum,
  url,
  embedCode,
  bildURL,
  bildName
) {
  // Array für 0..* Kategorien
  this.kategorien = [];
  // Array für 0..* Kapitel
  this.kapitelliste = [];

  this.name = name;
  this.sprache = sprache;
  this.beschreibung = beschreibung;
  this.dauer = getDauerInStundenUndMinuten(dauer);
  this.datum = datum;
  this.url = url;
  this.embedCode = embedCode;
  this.Bild = new Bild(bildURL, bildName);

  this.fuegeKategorieHinzu = function (kat) {
    this.kategorien.push(kat);
  };
  this.fuegKapitelHinzu = function (kap) {
    this.kapitelliste.push(kap);
  };
}

function Bild(url, name) {
  this.url = url;
  this.name = name;
}

function Kapitel(name, beschreibung, dauer) {
  this.name = name;
  this.beschreibung = beschreibung;
  this.dauer = getDauerInStundenUndMinuten(dauer);
}

// Nicht objektspezifische Hilfsfunktion (1)
function getDauerInStundenUndMinuten(dauer) {
  return `${dauer.split(":")[0]} Std. ${dauer.split(":")[1]} Min.`;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    Praktikum 08 Aufgabe 2 - Objekte erzeugen, sortieren und ausgeben
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
// Aufgabe 2.1
let kat1 = new Kategorie("Lowers", "assets/img/nice_cock.png", "Wizard");
let kat2 = new Kategorie("Uppers", "assets/img/nice_cock.png", "Wizard");
let kat3 = new Kategorie("Sits", "assets/img/nice_cock.png", "Wizard");
let kat4 = new Kategorie("Creative", "assets/img/nice_cock.png", "Wizard");

let tut1 = new Tutorial(
  "Around the world (atw)",
  "GER/DE",
  "Lerne einen der drei Basic-Tricks im Lower-Freestyle!",
  "01:52",
  "03.05.2023",
  "../../tutorial",
  null,
  "../img/nice_cock.png",
  "Wizard"
);
let tut2 = new Tutorial(
  "Half around the world (hatw)",
  "GER/DE",
  "Lerne einen der drei Basic-Tricks im Lower-Freestyle!",
  "02:15",
  "04.05.2023",
  "../../tutorial",
  null,
  "../img/nice_cock.png",
  "Wizard"
);

let kap1 = new Kapitel("Einleitung", "Einleitende Worte zum Thema", "00:17");
let kap2 = new Kapitel("Grundlagen", "Was man bereits können sollte", "00:19");
let kap3 = new Kapitel("1. Schritt", "Der richtige Anfang", "00:25");

tut1.fuegKapitelHinzu(kap1);
tut1.fuegKapitelHinzu(kap2);
tut1.fuegKapitelHinzu(kap3);

tut2.fuegKapitelHinzu(kap1);
tut2.fuegKapitelHinzu(kap2);
tut2.fuegKapitelHinzu(kap3);

tut1.fuegeKategorieHinzu(kat1);
tut2.fuegeKategorieHinzu(kat1);

let kategorienArr = [kat1, kat2, kat3, kat4];
let tutorialsArr = [tut1, tut2];

// Aufgabe 2.2
kategorienArr.sort((a, b) => a.name.localeCompare(b.name));

// Aufgabe 2.3
function getTutorialsZuKategorie(kategorieName) {
  let tutorials = [];
  tutorialsArr.forEach((tutorial) => {
    tutorial.kategorien.forEach((kategorie) => {
      if (kategorie.name.toUpperCase() === kategorieName.toUpperCase()) {
        tutorials.push(tutorial);
      }
    });
  });
  return tutorials;
}

// Aufgabe 2.4
kategorienArr.forEach((kategorie) => {
  console.log(`Kategorie: ${kategorie.name}`);
  console.log(`Bild: ${kategorie.Bild.name}`);
  getTutorialsZuKategorie(kategorie.name).forEach((tutorial) => {
    console.log("------------------------------------------------------------");
    console.log(
      `${tutorial.name} - (${tutorial.sprache})    ${tutorial.datum}`
    );
    console.log();
    console.log(`${tutorial.beschreibung}`);
    console.log();
    console.log(`${tutorial.dauer}`);
    console.log();
    if (tutorial.embedCode !== null) {
      console.log(`${tutorial.embedCode}`);
    } else {
      console.log(`${tutorial.url}`);
    }
    console.log();
    tutorial.kapitelliste.forEach((kapitel) => {
      console.log(
        `${kapitel.dauer}    ${kapitel.name}: ${kapitel.beschreibung}`
      );
    });
  });
  console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
});
