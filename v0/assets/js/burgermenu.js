/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Praktikum 09 Aufgabe 2 - Burger-Menü
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

let nav = document.querySelector('nav');
let anchors = document.querySelectorAll('nav a');
let clicked = false;

let div = document.createElement('div');

let burgermenu = document.createElement('img');
burgermenu.src = './assets/img/burger_menu.png';
burgermenu.alt = 'Nice cock!';
burgermenu.width = 30;
burgermenu.height = 30;
burgermenu.style.marginRight = '5px';

window.addEventListener('resize', () => {
    if (window.innerWidth <= 320 && clicked === true) {
        // Do nothing
    } else if (window.innerWidth <= 320) {
        anchors.forEach( (anchor) => anchor.remove());
        nav.style.justifyContent = 'flex-start';
        nav.append(burgermenu);
    } else {
        burgermenu.remove();
        nav.style.flexDirection = 'row';
        nav.style.justifyContent = 'space-evenly';
        anchors.forEach( (anchor) => nav.append(anchor));
        clicked = false;
    }
});

burgermenu.addEventListener('mouseover', () => {
    burgermenu.style.cursor = 'pointer';
});

burgermenu.addEventListener('click', () => {
    if (clicked === false) {
        div.style.display = 'flex';
        div.style.flexDirection = 'column';
        nav.append(div);
        anchors.forEach( (anchor) => div.append(anchor));
        nav.style.flexDirection = 'row';
        nav.style.justifyContent = 'flex-start';
        nav.style.alignItems = 'center';
        clicked = true;
    } else {
        div.remove();
        clicked = false;
    }
});
