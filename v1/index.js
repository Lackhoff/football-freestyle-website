const http = require("http");
const data = require("./models/persistence");
const url = require("url");

http.createServer((req, res) => {
    let queryParams = url.parse(req.url, true).query;
    let queryMatch = [];
    data.tutorials.forEach((tutorial) => {
        if (tutorial.name.toUpperCase().includes(queryParams["search"]?.toUpperCase()) && queryParams["search"] !== "") {
            queryMatch.push(tutorial);
        }
    });
    let varHTML = ``;
    if (queryMatch.length !== 0) {
        queryMatch.forEach((match) => {
            varHTML += `<li><a href="http://localhost:5500/tutorial?name=${match.name}">${match.name}</a> (${match.datum})</li>`;
        });
    } else {
        varHTML = `<p>Keine Tutorials gefunden!</p>`;
    }
    let html = `<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sebastian Lackhoff - Football Freestyle</title>
    <link rel="stylesheet" type="text/css" href="http://localhost:5500/assets/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="http://localhost:5500/assets/css/forms.css" />
    <link rel="stylesheet" type="text/css" href="http://localhost:5500/assets/css/grid.css" />
    <!--Autor-->
  </head>
  <body>
    <!--Kopfbereich-->
    <header>
      <h1>Tutorials</h1>
      <form>
        <label for="search">Suche:</label>
        <input
          class="search"
          id="search"
          name="search"
          placeholder="z. B. Tutorials"
        />
        <input class="search button" type="submit" value="Suchen!" />
      </form>
    </header>

    <!--Navigationsbereich-->
    <nav>
      <a href="http://localhost:8020">Startseite</a>
      <a href="http://localhost:8020/registration">Registrierung</a>
    </nav>

    <!--Hauptbereich-->
    <main>
      <section>
        <h2>Tutorials mit: ${queryParams["search"]}</h2>
        <ul>
          ${varHTML}
        </ul>
      </section>
    </main>

    <!--Seitenbereich-->
    <aside>
      <h3>Neue Tutorials</h3>
      <ul>
        <li>Around the world: 03.05.2023 - Dauer: 1 Min. 52 Sek.</li>
        <li>Hop the world: 04.05.2023 - Dauer: 2 Min. 20 Sek.</li>
      </ul>
    </aside>

    <!--Fußbereich-->
    <footer>&copy; Sebastian Lackhoff 2023</footer>

    <!--Skripte-->
    <script src="http://localhost:5500/assets/js/burgermenu.js"></script>
  </body>
</html>`;
    res.writeHead(200, {"content-type" : "text/html; charset=utf-8"});
    res.end(html);
}).listen(8844, () => {
    console.log("Web-Anwendung mit Klick auf folgenden Link starten: http://localhost:8844");
});
