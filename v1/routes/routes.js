const express = require("express");
const router = express.Router();
const data = require("../models/persistence");
const url = require("url");

router.use("/assets", express.static("public"));
router.use(express.urlencoded({ extended: false }));

router.get("/", (req, res) => {
  res.render("start");
});

router.get("/tutorials", (req, res) => {
  let tutorials = data.getTutorialsZuKategorie(req.query.category);
  res.render("tutorials", { tutorials });
});

router.get("/tutorial", (req, res) => {
  let tutorial = data.tutorials.find(
    (tutorial) => tutorial.name === req.query.name
  );
  res.render("tutorial", { tutorial });
});

router.get("/registration", (req, res) => {
  res.render("registration");
});

router.post("/registration", (req, res) => {
  let user = new data.User(
    req.body.name,
    req.body.firstname,
    req.body.birthday,
    req.body.e - mail
  );
  data.user.push(user);
  res.redirect("/");
});

router.get("/login", (req, res) => {
  res.render("login");
});

router.get("/search", (req, res) => {
  let queryParams = url.parse(req.url, true).query;
  let queryMatch = [];
  data.tutorials.forEach((tutorial) => {
    if (
      tutorial.name
        .toUpperCase()
        .includes(queryParams["search"]?.toUpperCase()) &&
      queryParams["search"] !== ""
    ) {
      queryMatch.push(tutorial);
    }
  });
  res.render("search", { queryParams, queryMatch });
});

router.get("/privacy_police_publisher", (req, res) => {
  res.render("privacy_police_publisher");
});

router.use((req, res) => {
  res.status(404).render("error");
});

module.exports = router;
