let link = document.querySelector("#appearance");

function lightmode() {
  link.href = "/assets/css/lightmode.css";
}

function darkmode() {
  link.href = "/assets/css/darkmode.css";
}
