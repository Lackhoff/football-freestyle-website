/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Praktikum 10 Aufgabe 1.1.3.1 - Fachobjekte
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

function Kategorie(name, bildURL, bildName) {
    this.name = name;
    this.Bild = new Bild(bildURL, bildName);
  }
  
  function Tutorial(
    name,
    sprache,
    beschreibung,
    dauer,
    datum,
    url,
    embedCode,
    bildURL,
    bildName
  ) {
    // Array für 0..* Kategorien
    this.kategorien = [];
    // Array für 0..* Kapitel
    this.kapitelliste = [];
  
    this.name = name;
    this.sprache = sprache;
    this.beschreibung = beschreibung;
    this.dauer = getDauerInStundenUndMinuten(dauer);
    this.datum = datum;
    this.url = url;
    this.embedCode = embedCode;
    this.Bild = new Bild(bildURL, bildName);
  
    this.fuegeKategorieHinzu = function (kat) {
      this.kategorien.push(kat);
    };
    this.fuegKapitelHinzu = function (kap) {
      this.kapitelliste.push(kap);
    };
  }
  
  function Bild(url, name) {
    this.url = url;
    this.name = name;
  }
  
  function Kapitel(name, beschreibung, dauer) {
    this.name = name;
    this.beschreibung = beschreibung;
    this.dauer = getDauerInStundenUndMinuten(dauer);
  }

  function User(lastname, firstname, birthday, email) {
    this.lastname = lastname;
    this.firstname = firstname;
    this.birthday = birthday;
    this.email = email;
  }
  
  // Nicht objektspezifische Hilfsfunktion (1)
  function getDauerInStundenUndMinuten(dauer) {
    return `${dauer.split(":")[0]} Min. ${dauer.split(":")[1]} Sek.`;
  }
  // Praktikum 08 Aufgabe 2.1
  let kat1 = new Kategorie("Lowers", "assets/img/nice_cock.png", "Wizard");
  let kat2 = new Kategorie("Uppers", "assets/img/nice_cock.png", "Wizard");
  let kat3 = new Kategorie("Sits", "assets/img/nice_cock.png", "Wizard");
  let kat4 = new Kategorie("Creative", "assets/img/nice_cock.png", "Wizard");
  
  let tut1 = new Tutorial(
    "Around the world (atw)",
    "GER/DE",
    "Lerne einen der drei Basic-Tricks im Lower-Freestyle!",
    "01:52",
    "03.05.2023",
    "../../tutorial",
    null,
    "../img/nice_cock.png",
    "Wizard"
  );
  let tut2 = new Tutorial(
    "Half around the world (hatw)",
    "GER/DE",
    "Lerne einen der drei Basic-Tricks im Lower-Freestyle!",
    "02:15",
    "04.05.2023",
    "../../tutorial",
    null,
    "../img/nice_cock.png",
    "Wizard"
  );

  let tut3 = new Tutorial(
    "Hop the world (htw)",
    "GER/DE",
    "Lerne einen der drei Basic-Tricks im Lower-Freestyle!",
    "02:20",
    "27.07.2023",
    "../../tutorial",
    null,
    "../img/nice_cock.png",
    "Wizard"
  );

  let tut4 = new Tutorial(
    "Neck stall",
    "GER/DE",
    "Lerne einen der Basic-Tricks im Upper-Freestyle!",
    "02:10",
    "27.07.2023",
    "../../tutorial",
    null,
    "../img/nice_cock.png",
    "Wizard"
  );

  let tut5 = new Tutorial(
    "Sole stall",
    "GER/DE",
    "Lerne einen der Basic-Tricks im Sit-Freestyle!",
    "02:35",
    "27.07.2023",
    "../../tutorial",
    null,
    "../img/nice_cock.png",
    "Wizard"
  );

  let tut6 = new Tutorial(
    "Dragon stall",
    "GER/DE",
    "Lerne einen coolen Trick des Creative-Freestyle!",
    "02:35",
    "27.07.2023",
    "../../tutorial",
    null,
    "../img/nice_cock.png",
    "Wizard"
  );

  let tut7 = new Tutorial(
    "Jordan stall",
    "GER/DE",
    "Lerne einen sehr anspruchsvollen Trick des Creative-Freestyle!",
    "03:05",
    "27.07.2023",
    "../../tutorial",
    null,
    "../img/nice_cock.png",
    "Wizard"
  );
  
  let kap1 = new Kapitel("Einleitung", "Einleitende Worte zum Thema", "00:17");
  let kap2 = new Kapitel("Grundlagen", "Was man bereits können sollte", "00:19");
  let kap3 = new Kapitel("1. Schritt", "Der richtige Anfang", "00:25");
  
  tut1.fuegKapitelHinzu(kap1);
  tut1.fuegKapitelHinzu(kap2);
  tut1.fuegKapitelHinzu(kap3);
  
  tut2.fuegKapitelHinzu(kap1);
  tut2.fuegKapitelHinzu(kap2);
  tut2.fuegKapitelHinzu(kap3);
  
  tut1.fuegeKategorieHinzu(kat1);
  tut2.fuegeKategorieHinzu(kat1);
  tut3.fuegeKategorieHinzu(kat1);
  tut4.fuegeKategorieHinzu(kat2);
  tut5.fuegeKategorieHinzu(kat3);
  tut6.fuegeKategorieHinzu(kat4);
  tut7.fuegeKategorieHinzu(kat4);
  
  let kategorienArr = [kat1, kat2, kat3, kat4];
  let tutorialsArr = [tut1, tut2, tut3, tut4, tut5, tut6, tut7];
  let userArr = [];
  
  // Praktikum 08 Aufgabe 2.2
  kategorienArr.sort((a, b) => a.name.localeCompare(b.name));
  
  // Praktikum 08 Aufgabe 2.3
  function getTutorialsZuKategorie(kategorieName) {
    let tutorials = [];
    tutorialsArr.forEach((tutorial) => {
      tutorial.kategorien.forEach((kategorie) => {
        if (kategorie.name?.toUpperCase() === kategorieName?.toUpperCase()) {
          tutorials.push(tutorial);
        }
      });
    });
    return tutorials;
  }

module.exports = {
    kategorien: kategorienArr,
    tutorials: tutorialsArr,
    user: userArr,
    getDauer: getDauerInStundenUndMinuten,
    getTutorialsZuKategorie: getTutorialsZuKategorie
};
