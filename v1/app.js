const express = require("express");
const app = express();
const router = require("./routes/routes");

app.set("view engine", "ejs");
app.set("views", "views");

app.use("/", router);

app.listen(8020, () => {
    console.log("Web-Anwendung mit Klick auf folgenden Link starten: http://localhost:8020");
});
